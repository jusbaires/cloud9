<?php
echo "Instando 'ftp'\n";
shell_exec('

sudo apt-get update

sudo apt-get -y install -y --force-yes debconf-utils
sudo sh -c "echo \"proftpd-basic shared/proftpd/inetd_or_standalone select standalone\" | debconf-set-selections"
sudo apt-get -y install -y --force-yes proftpd-basic

sudo bash -c "cat >>/etc/proftpd/conf.d/anonymous.conf <<EOF
<Anonymous ~ftp>
  User                                ftp
  Group                               nogroup
  UserAlias                   anonymous ftp
  DirFakeUser on ftp
  DirFakeGroup on ftp
  RequireValidShell           off
  MaxClients                  10
  DisplayLogin                        welcome.msg
  DisplayChdir                .message
</Anonymous>
EOF"

sudo service proftpd restart

sudo bash -c "cat >>~/.profile <<EOF

# Configuracion de FTP.
export CI_CONFIG_FTP_HOSTNAME=localhost
export CI_CONFIG_FTP_USERNAME
export CI_CONFIG_FTP_PASSWORD
export CI_CONFIG_FTP_PORT=21

EOF"

source ~/.profile

wget https://bitbucket.org/jusbaires/cloud9/downloads/net2ftp_v1.0_light.zip -O /tmp/net2ftp_v1.0_light.zip

sudo unzip /tmp/net2ftp_v1.0_light.zip -d /opt

sudo bash -c "cat >>/etc/apache2/conf-available/net2ftp.conf <<EOF

Alias /net2ftp /opt/net2ftp_v1.0_light/files_to_upload/
<Directory /opt/net2ftp_v1.0_light/files_to_upload>
    Options FollowSymLinks
    Require all granted
</Directory>

EOF"

sudo a2enconf net2ftp
sudo service apache2 reload


');


