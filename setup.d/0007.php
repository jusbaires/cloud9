<?php
echo "Instando 'jusbaires.source.server'\n";
shell_exec('

wget -O - https://raw.githubusercontent.com/alberthier/git-webui/master/install/installer.sh | bash

sudo a2enmod proxy
sudo a2enmod proxy_http
sudo a2enmod proxy_ajp
sudo a2enmod rewrite
sudo a2enmod deflate
sudo a2enmod headers
sudo a2enmod proxy_balancer
sudo a2enmod proxy_connect
sudo a2enmod proxy_html

sudo bash -c "cat >> /etc/apache2/sites-available/001-cloud9.conf <<EOF

#configuracion de git-webui
<VirtualHost *:8080>
    ServerName git-webui.\\\\\\${C9_HOSTNAME}
    ProxyPreserveHost Off 
    ProxyPass / http://localhost:8000/
    ProxyPassReverse / http://localhost:8000/
</VirtualHost>

EOF"

sudo bash -c "cat >> /usr/local/bin/jusbaires.source.server <<EOF
#!/bin/sh
cd ~/workspace
git webui
EOF"
sudo chmod +x /usr/local/bin/jusbaires.source.server

sudo service apache2 restart

');
