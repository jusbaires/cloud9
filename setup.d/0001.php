<?php

echo "Configurando 'apache'";

$tmpfile = tempnam("","");
file_put_contents($tmpfile, "
<VirtualHost *:8080>
    DocumentRoot /home/ubuntu/workspace/content
    ServerName https://\${C9_HOSTNAME}:443

    LogLevel info

    ErrorLog \${APACHE_LOG_DIR}/error.log
    CustomLog \${APACHE_LOG_DIR}/access.log combined

    <Directory /home/ubuntu/workspace/content>
        Options Indexes FollowSymLinks
        AllowOverride All
        Require all granted
        
        RewriteEngine On
        RewriteBase /
        RewriteCond %{REQUEST_FILENAME} !-f
        RewriteCond %{REQUEST_FILENAME} !-d
        RewriteRule ^(.*)$ index.php?/ [L]
        
        SetEnv CI_BASE_URL https://\${C9_HOSTNAME}/
        SetEnv CI_SYSTEM_PATH /usr/local/lib/code-igniter
        SetEnv CI_CONFIG_DATABASE_HOSTNAME \${IP}
	    SetEnv CI_CONFIG_DATABASE_USERNAME \${C9_USER}
	    SetEnv CI_CONFIG_DATABASE_PASSWORD
	    SetEnv CI_CONFIG_DATABASE_DATABASE c9
        
        
    </Directory>
</VirtualHost>
ServerName https://\${C9_HOSTNAME}
# vim: syntax=apache ts=4 sw=4 sts=4 sr noet
");

shell_exec("sudo rm /etc/apache2/sites-available/001-cloud9.conf");
shell_exec("sudo mv $tmpfile /etc/apache2/sites-available/001-cloud9.conf");
shell_exec("sudo service apache2 restart");

echo "Configurando 'profile'\n";
file_put_contents("/home/ubuntu/.profile", "

export CI_BASE_URL=https://\${C9_HOSTNAME}/
export CI_SYSTEM_PATH=/usr/local/lib/code-igniter
export CI_CONFIG_DATABASE_HOSTNAME=\${IP}
export CI_CONFIG_DATABASE_USERNAME=\${C9_USER}
export CI_CONFIG_DATABASE_PASSWORD=\"\"
export CI_CONFIG_DATABASE_DATABASE=c9

",  FILE_APPEND);

echo "Instalando paquete 'mysql'\n";
shell_exec("mysql-ctl start");

echo "Instalando paquete 'phpmyadmin'\n";
shell_exec("phpmyadmin-ctl install");

echo "Instalando paquete 'codeigniter'\n";
$tmpfile = tempnam("","");
shell_exec("curl -s -o $tmpfile https://codeload.github.com/bcit-ci/CodeIgniter/zip/3.0.0");
shell_exec("unzip $tmpfile CodeIgniter-3.0.0/system/* -d $tmpfile-unzip");
shell_exec("sudo mv $tmpfile-unzip/CodeIgniter-3.0.0/system /usr/local/lib/code-igniter");
shell_exec("rm -rf $tmpfile-unzip");
unlink($tmpfile);

echo "Instalando comando 'jusbaires.upgrade'\n";
$tmpfile = tempnam("","");
file_put_contents($tmpfile, "#!/bin/sh
curl -s https://bitbucket.org/jusbaires/cloud9/raw/master/setup.php | php
");
system("sudo mv $tmpfile /usr/local/bin/jusbaires.upgrade");
system("sudo chmod +x /usr/local/bin/jusbaires.upgrade");

echo "Instalando comando 'jusbaires.newproject'\n";
$tmpfile = tempnam("","");
file_put_contents($tmpfile, "#!/bin/sh
curl -s https://bitbucket.org/jusbaires/cloud9/raw/master/setup.d/files/empty.tar.gz | tar -xz
");
system("sudo mv $tmpfile /usr/local/bin/jusbaires.newproject");
system("sudo chmod +x /usr/local/bin/jusbaires.newproject");


echo "Instalando comando 'jusbaires.migration.restart'\n";
$tmpfile = tempnam("","");
file_put_contents($tmpfile, "#!/bin/bash
source ~/.profile
php ~/workspace/content/index.php migrate restart
");
system("sudo mv $tmpfile /usr/local/bin/jusbaires.migration.restart");
system("sudo chmod +x /usr/local/bin/jusbaires.migration.restart");

echo "Instalando comando 'jusbaires.migration.update'\n";
$tmpfile = tempnam("","");
file_put_contents($tmpfile, "#!/bin/bash
source ~/.profile
php ~/workspace/content/index.php migrate update
");
system("sudo mv $tmpfile /usr/local/bin/jusbaires.migration.update");
system("sudo chmod +x /usr/local/bin/jusbaires.migration.update");



