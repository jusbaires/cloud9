<?php

#constante que define la ultima version publicada del entorno
$latest = 9;

#ruta al archivo que almacena la version actual del entorno
$verpath = getenv('HOME').DIRECTORY_SEPARATOR.'.version';

#obtiene la version actual del entorno
$current = (file_exists($verpath)) ? intval(file_get_contents($verpath)) : 0;

#por cada version no ejecutada aun
for($i = $current; $i < $latest; $i++) {
	#ejecuta el script de version correspondiente
	system('curl -s https://bitbucket.org/jusbaires/cloud9/raw/master/setup.d/'.str_pad($i+1, 4, "0", STR_PAD_LEFT).'.php | php');
}

#guarda el numero de vesion actual
file_put_contents($verpath, $latest);
